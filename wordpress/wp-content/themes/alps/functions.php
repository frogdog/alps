<?php

function alps_enqueue() {
  	//remove dashicons
  	if(!is_user_logged_in()) {
   		wp_deregister_style( 'dashicons' ); 
   		wp_dequeue_style('et-core-main-fonts');
   	}

   	//load divi parents
   	wp_enqueue_style('divi-parent', get_template_directory_uri() . '/style.css');

    //load theme script
    wp_enqueue_script( 
    	'alps-scripts', 
    	get_stylesheet_directory_uri() . '/js/alps.min.js', 
    	array('jquery'), 
    	'1.0.0', 
    	true 
    );
}
add_action( 'wp_enqueue_scripts', 'alps_enqueue' );