var gulp = require('gulp'),
  browserSync = require('browser-sync'),
  sass = require('gulp-sass'),
  prefix = require('gulp-autoprefixer'),
  plumber = require('gulp-plumber'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  cleanCSS = require('gulp-clean-css'),
  postcss = require('gulp-postcss'),
  sourcemaps = require('gulp-sourcemaps');

/**
 * Default task, running just `gulp` will
 * compile Sass files, launch BrowserSync & watch files.
 */

gulp.task('build',
  gulp.series(javascript, styles)
  );

gulp.task('default',
  gulp.series('build', server, watch)
  );

function styles(){
  var css = {
    processors: [
      require('autoprefixer') ({
        browsers: ['last 2 versions', '> 2%']
      }),
      require('css-mqpacker'),
      require('cssnano')
    ]
  }
  return gulp.src('style.scss')
    .pipe(plumber())
    // .pipe(sourcemaps.init())
    .pipe(sass({outputStyle:'compressed'}).on('error', sass.logError))
    .pipe(postcss(css.processors))
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
    .pipe(browserSync.reload({stream: true}));
}

function javascript() {
  return gulp.src(['js/*.js', '!js/*.min.js'])
  .pipe(plumber())
  .pipe(uglify())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('js'))
}


function reload(done) {
  browserSync.reload();
  done();
}

/**
 * @task watch
 * Watch scss files for changes & recompile
 * Clear cache when Drupal related files are changed
 */
function watch() {
  gulp.watch(['js/*.js', '!js/*.min.js']).on('all', gulp.series(javascript, browserSync.reload));
  gulp.watch(['*.scss', 'scss/**/*.scss']).on('all', styles);
  gulp.watch('**/*.{php,inc,info}').on('all', browserSync.reload);
}

/**
 * Launch the Server
 */
function server(done) {
  browserSync.init({
    proxy: "alps.lndo.site",
    socket: {
      domain: 'localhost:3000'
    }
  }, done);
}
